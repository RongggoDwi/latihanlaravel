<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index');
    }

    public function list()
    {
        return view('list.index');
    }

    public function examp()
    {
        return view('examp.examp');
    }

    public function contact()
    {
        return view('contact.contact');
    }
}
