<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">Product Store</div>
        <div class="links">
            <a href="{{ config('app.url')}}/product/createproduct">Create Product</a>
            <a href="{{ config('app.url')}}/product/viewproducts">View Products</a>
        </div>
    </div>
</div>