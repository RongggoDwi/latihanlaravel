<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <title>@yield('title')</title>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm">
    <div class="container">
      <a class="navbar-brand" href="#">Ronggo Dwi Wahono</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/list">List</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/examp">Example</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Akhir Navbar -->

  <!-- Jumbotron -->
  <section class="jumbotron text-center">
    <div>
      @yield('content')
    </div>
    <!-- <img src="images/profile1.png" alt="Ronggo Dwi W" width="200" class="rounded-circle"> -->
    <!-- <h1 class="display-4"></h1>
        <p class="lead"></p> -->
  </section>

  <!-- Jumbotron Akhir -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script> -->

  <!-- About -->
  <!-- <div class="container"> -->
</body>

<footer>
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
    <path fill="#ff5500" fill-opacity="1" d="M0,160L8,144C16,128,32,96,48,96C64,96,80,128,96,122.7C112,117,128,75,144,69.3C160,64,176,96,192,138.7C208,181,224,235,240,245.3C256,256,272,224,288,202.7C304,181,320,171,336,144C352,117,368,75,384,90.7C400,107,416,181,432,229.3C448,277,464,299,480,266.7C496,235,512,149,528,101.3C544,53,560,43,576,80C592,117,608,203,624,224C640,245,656,203,672,181.3C688,160,704,160,720,165.3C736,171,752,181,768,192C784,203,800,213,816,192C832,171,848,117,864,90.7C880,64,896,64,912,85.3C928,107,944,149,960,160C976,171,992,149,1008,133.3C1024,117,1040,107,1056,138.7C1072,171,1088,245,1104,261.3C1120,277,1136,235,1152,229.3C1168,224,1184,256,1200,229.3C1216,203,1232,117,1248,90.7C1264,64,1280,96,1296,122.7C1312,149,1328,171,1344,160C1360,149,1376,107,1392,96C1408,85,1424,107,1432,117.3L1440,128L1440,320L1432,320C1424,320,1408,320,1392,320C1376,320,1360,320,1344,320C1328,320,1312,320,1296,320C1280,320,1264,320,1248,320C1232,320,1216,320,1200,320C1184,320,1168,320,1152,320C1136,320,1120,320,1104,320C1088,320,1072,320,1056,320C1040,320,1024,320,1008,320C992,320,976,320,960,320C944,320,928,320,912,320C896,320,880,320,864,320C848,320,832,320,816,320C800,320,784,320,768,320C752,320,736,320,720,320C704,320,688,320,672,320C656,320,640,320,624,320C608,320,592,320,576,320C560,320,544,320,528,320C512,320,496,320,480,320C464,320,448,320,432,320C416,320,400,320,384,320C368,320,352,320,336,320C320,320,304,320,288,320C272,320,256,320,240,320C224,320,208,320,192,320C176,320,160,320,144,320C128,320,112,320,96,320C80,320,64,320,48,320C32,320,16,320,8,320L0,320Z"></path>
  </svg>
</footer>
<!-- </div> -->
<!-- About akhir -->

</html>