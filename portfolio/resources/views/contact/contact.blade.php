@extends('layout.master')
@section('title', 'My Profil | Ronggo Dwi')
@section('content')
<br>
<div class="mb-3 row">
    <div class="mb-3 row">
        <label for="comments" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword">
        </div>
        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
        </div>
    </div>
    <div class="mb-3 row">
        <label for="inputPassword" class="col-sm-2 col-form-label">Phone</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputPassword">
        </div>
    </div>
    <div class="mb-3 row">
        <label for="floatingTextarea2" class="col-sm-2 col-form-label">Address</label>
        <div class="col-sm-10">
            <textarea class="form-control" placeholder="Leave your address here" id="floatingTextarea2" style="height: 120px"></textarea>
        </div>
    </div>
</div>
<br>
@endsection