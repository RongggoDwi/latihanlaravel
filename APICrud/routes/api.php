<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/productcategories', 'ProductCategoriesApiController@getAllProductCategories'); // fix
Route::get('/productcategories/{id}', 'ProductCategoriesApiController@getProductCategoriesById');
Route::post('/productcategories/add', 'ProductCategoriesApiController@saveProductCategories');
Route::put('/productcategories/edit/{id}', 'ProductCategoriesApiController@updateProductCategories');
Route::delete('/productcategories/delete/{id}', 'ProductCategoriesApiController@deleteProductCategories');

Route::get('/products', 'ProductsApiController@getAllProducts'); // fix
Route::get('/products/{id}', 'ProductsApiController@getProductsById');
Route::post('/products/add', 'ProductsApiController@saveProducts');
Route::put('/products/edit/{id}', 'ProductsApiController@updateProducts');
Route::delete('/products/delete/{id}', 'ProductsApiController@deleteProducts');

Route::get('/customers', 'CustomersApiController@getAllCustomers'); // fix
Route::get('/customers/{id}', 'CustomersApiController@getCustomersById');
Route::post('/customers/add', 'CustomersApiController@saveCustomers');
Route::put('/customers/edit/{id}', 'CustomersApiController@updateCustomers');
Route::delete('/customers/delete/{id}', 'CustomersApiController@deleteCustomers');

Route::get('/orders', 'OrdersApiController@getAllOrders'); // fix
Route::get('/orders/{id}', 'OrdersApiController@getOrdersById');
Route::post('/orders/add', 'OrdersApiController@saveOrders');
Route::put('/orders/edit/{id}', 'OrdersApiController@updateOrders');
Route::delete('/orders/delete/{id}', 'OrdersApiController@deleteOrders');

Route::get('/orderitems', 'OrdersItemApiController@getAllOrderItems'); // fix
Route::get('/orderitems/{id}', 'OrdersItemApiController@getOrderItemsById');
Route::post('/orderitems/add', 'OrdersItemApiController@saveOrderItems');
Route::put('/orderitems/edit/{id}', 'OrdersItemApiController@updateOrderItems');
Route::delete('/orderitems/delete/{order_id}/{item_id}', 'OrdersItemApiController@deleteOrderItems');

Route::get('/sales', 'SalesApiController@getAllSales'); //fix

Route::get('/simple', 'SimpleController@getData');
Route::get('/simple/{id}', 'SimpleController@getDataById');
Route::post('/simple', 'SimpleController@saveData');
Route::put('/simple/{id}', 'SimpleController@updateData');
Route::delete('/simple/{id}', 'SimpleController@deleteData'); //fix
