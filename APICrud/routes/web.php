<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/simple', 'SimpleController@index');
Route::get('/simple/add', 'SimpleController@add');
Route::post('/simple/simpan', 'SimpleController@simpan');

Route::get('/simple/edit/{id}', 'SimpleController@edit');
Route::post('/simple/editdata', 'SimpleController@editdata');

Route::get('/simple/datadelete/{id}', 'SimpleController@datadelete');
Route::post('/simple/deleted', 'SimpleController@deleted');
