<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders_item extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'batch258.order_items';
    protected $primaryKey = 'order_id';
}
