<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders_item;
use Illuminate\Support\Facades\DB;

class OrdersItemApiController extends Controller
{
    public function getAllOrderItems()
    {
        $data = DB::select('SELECT * FROM BATCH258.order_items');
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function getOrderItemsById($id)
    {
        $data = DB::select('SELECT * FROM BATCH258.order_items WHERE order_id = ?', array($id));
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - noData";
        }
        return response($res);
    }

    public function saveOrderItems(Request $req)
    {
        $data = DB::update('INSERT INTO BATCH258.order_items VALUES (?, ?, ?, ?, ?)', array($req->order_id, $req->item_id, $req->product_id, $req->quantity, $req->unit_price));
        if ($data) {
            $res['status'] = "200 - Success Insert";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }

    public function updateOrderItems(Request $req, $id)
    {
        $data = DB::update('UPDATE BATCH258.order_items SET item_id = ?, product_id = ?, quantity = ?, unit_price = ? WHERE order_id = ?', array($req->item_id, $req->product_id, $req->quantity, $req->unit_price, $id));
        if ($data) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function deleteOrderItems($order_id, $item_id)
    {
        $data = Orders_item::find($order_id)->find($item_id)->first();

        if ($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }
}
