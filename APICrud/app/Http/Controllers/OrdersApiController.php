<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use Illuminate\Support\Facades\DB;

class OrdersApiController extends Controller
{
    public function getAllOrders()
    {
        $data = DB::select('SELECT * FROM BATCH258.orders');
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function getOrdersById($id)
    {
        $data = DB::select('SELECT * FROM BATCH258.orders WHERE order_id = ?', array($id));
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No-Data";
        }
        return response($res);
    }

    public function saveOrders(Request $req)
    {
        $data = DB::update('INSERT INTO BATCH258.orders VALUES (?, ?, ?, ?, ?)', array($req->order_id, $req->customer_id, $req->status, $req->salesman_id, $req->order_date));
        if ($data) {
            $res['status'] = "200 - Success Insert";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }

    public function updateOrders(Request $req, $id)
    {
        $data = DB::update('UPDATE BATCH258.orders SET customer_id = ?, status = ?, salesman_id = ?, order_date = ? WHERE order_id = ?', array($req->customer_id, $req->status, $req->salesman_id, $req->order_date, $id));
        if ($data) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function deleteOrders($id)
    {
        $data = DB::delete('DELETE FROM BATCH258.orders WHERE order_id = ?', array($id));
        if ($data) {
            $res['status'] = "200 - Success Delete";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }
}
