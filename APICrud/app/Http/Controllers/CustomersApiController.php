<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customers;
use Illuminate\Support\Facades\DB;

class CustomersApiController extends Controller
{
    public function getAllCustomers()
    {
        $data = DB::select('SELECT * FROM BATCH258.customers');
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function getCustomersById($id)
    {
        $data = DB::select('SELECT * FROM BATCH258.customers WHERE customer_id = ?', array($id));
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - noData";
        }
        return response($res);
    }

    public function saveCustomers(Request $req)
    {
        $data = DB::update('INSERT INTO BATCH258.customers VALUES (?, ?, ?, ?, ?)', array($req->customer_id, $req->name, $req->address, $req->website, $req->credit_limit));
        if ($data) {
            $res['status'] = "200 - Success Insert";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }

    public function updateCustomers(Request $req, $id)
    {
        $data = DB::update('UPDATE BATCH258.customers SET name = ?, address = ?, website = ?, credit_limit = ? WHERE customer_id = ?', array($req->name, $req->address, $req->website, $req->credit_limit, $id));
        if ($data) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function deleteCustomers($id)
    {
        $data = DB::delete('DELETE FROM BATCH258.customers WHERE customer_id = ?', array($id));
        if ($data) {
            $res['status'] = "200 - Success Delete";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }
}
