<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use Illuminate\Support\Facades\DB;

class ProductsApiController extends Controller
{
    public function getAllProducts()
    {
        $data = DB::select('SELECT * FROM BATCH258.products');
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function getProductsById($id)
    {
        $data = DB::select('SELECT * FROM BATCH258.products WHERE product_id = ?', array($id));
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - noData";
        }
        return response($res);
    }

    public function saveProducts(Request $req)
    {
        $data = DB::update('INSERT INTO BATCH258.products VALUES (?, ?, ?, ?, ?, ?)', array($req->product_id, $req->product_name, $req->description, $req->standard_cost, $req->list_price, $req->category_id));
        if ($data) {
            $res['status'] = "200 - Success Insert";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }

    public function updateProducts(Request $req, $id)
    {
        $data = DB::update('UPDATE BATCH258.products SET product_name = ?, description = ?, standard_cost = ?, list_price = ?, category_id = ? WHERE product_id = ?', array($req->product_name, $req->description, $req->standard_cost, $req->list_price, $req->category_id, $id));
        if ($data) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function deleteProducts($id)
    {
        $data = DB::delete('DELETE FROM BATCH258.products WHERE product_id = ?', array($id));
        if ($data) {
            $res['status'] = "200 - Success Delete";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }
}
