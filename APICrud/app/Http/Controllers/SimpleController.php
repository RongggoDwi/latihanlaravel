<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Simple;

class SimpleController extends Controller
{
    public function getData()
    {
        $data = Simple::all();
        if (count($data) > 0) {
            $res['status'] = "200 - success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - noData";
        }
        return response($res);
    }

    public function getDataById($id)
    {
        $data = Simple::where('id', $id)->get();
        if (count($data) > 0) {
            $res['status'] = "200 - success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - noData";
        }
        return response($res);
    }

    public function saveData(Request $req)
    {
        $data = new Simple();
        $data->name = $req->name;
        $data->alamat = $req->alamat;
        if ($data->save()) {
            $res['status'] = "200 - success insert";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - service tidak tersedia";
        }
        return response($res);
    }

    public function updateData(Request $req, $id)
    {
        $data = Simple::find($id);
        $data->name = $req->name;
        $data->alamat = $req->alamat;
        if ($data->save()) {
            $res['status'] = "200 - success update";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - service tidak tersedia";
        }
        return response($res);
    }

    public function deleteData($id)
    {
        $data = Simple::find($id);
        if ($data->delete()) {
            $res['status'] = "200 - success delete";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - service tidak tersedia";
        }
        return response($res);
    }
}
