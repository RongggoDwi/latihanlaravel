<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesApiController extends Controller
{
    public function getAllSales()
    {
        // $data = DB::select('SELECT 
        //     batch258.terbaik.get_sales_value,
        //     batch258.terbaik.get_manager_by_sales FROM dual');
        $data = DB::select('SELECT batch258.getPenjualan() FROM dual');
        if (count($data) > 0) {
            $data['status'] = 'Success';
            $data['data'] = $data;
        } else {
            $data['status'] = 'No Data';
        }
        echo '<pre>';
        print_r($data);
        return response($data);
        // $data = DB::select("SELECT batch258.get_all_customers() FROM dual");
        // echo '<pre>';
        // print_r($data);
    }
}
