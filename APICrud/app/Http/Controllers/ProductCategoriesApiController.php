<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductCategories;
use Illuminate\Support\Facades\DB;

class ProductCategoriesApiController extends Controller
{
    public function getAllProductCategories()
    {
        $data = DB::select('SELECT * FROM BATCH258.product_categories');
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function getProductCategoriesById($id)
    {
        $data = DB::select('SELECT * FROM BATCH258.product_categories WHERE category_id = ?', array($id));
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - noData";
        }
        return response($res);
    }

    public function saveProductCategories(Request $req)
    {
        $data = DB::update('INSERT INTO BATCH258.product_categories VALUES (?, ?)', array($req->category_id, $req->category_name));
        if ($data) {
            $res['status'] = "200 - Success Insert";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }

    public function updateProductCategories(Request $req, $id)
    {
        $data = DB::update('UPDATE BATCH258.product_categories SET category_name = ? WHERE category_id = ?', array($req->category_name, $id));
        if ($data) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function deleteProductCategories($id)
    {
        $data = DB::delete('DELETE FROM BATCH258.product_categories WHERE category_id = ?', array($id));
        if ($data) {
            $res['status'] = "200 - Success Delete";
        } else {
            $res['status'] = "503 - service unavailable";
        }
        return response($res);
    }
}
