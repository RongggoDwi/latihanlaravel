<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SimpleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_simple')->insert([
            'name' => 'Ronggo Dwi Wahono',
            'alamat' => 'Jl. Jakenan pati',
        ]);
    }
}
