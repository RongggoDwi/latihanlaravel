<?php
ini_set("soap.wsdl_cache_enabled", "0");

class Book
{
    public $name;
    public $year;
}

$book = new Book();
$book->name = 'test 2';

$client = new SoapClient('http://localhost:7700/server.php?wsdl', array("trace" => 1, "exception" => 0));
$resp  = $client->bookYear($book);

var_dump($resp);
