<?php

try {

    $soapClient = new SoapClient('https://www.w3schools.com/xml/tempconvert.asmx?wsdl');

    $response = $soapClient->FahrenheitToCelsius();

    echo '<pre>';
    var_dump($response);

    echo '<br><br>';

    $array = json_decode(json_encode($response), true);

    print_r($array);

    echo '<br><br>';

    echo $array['FahrenheitToCelsiusResult']['Fahrenheit']['0'];
} catch (Exception $th) {
    echo $th->getMessage();
}
