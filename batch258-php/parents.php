<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<input type="hidden" id="id">
<div class="table-responsive">
    <table class="table">
        <tr>
            <td>NIK</td>
            <td> : <input type="text" id="nik"></td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td> : <input type="text" id="nama"></td>
        </tr>
        <tr>
            <td>TANGGAL_LAHIR</td>
            <td> : <input type="text" id="tanggal_lahir"></td>
        </tr>
        <tr>
            <td>ALAMAT</td>
            <td> : <input type="text" id="alamat"></td>
        </tr>
        <tr>
            <td>STATUS</td>
            <td> : <input type="text" id="status"></td>
        </tr>
        <tr>
            <td><button id="savedata" onclick="savedata()">Simpan</button>
                <button id="editdata" onclick="editdata()">Update</button>
            </td>
        </tr>
    </table>
</div>

<div id="tampildata"></div>

<script>
    listdata();

    function listdata() {
        $('#editdata').hide()
        $.ajax({
            url: 'http://localhost:8000/api/parents',
            type: 'get',
            contentType: 'application/json',
            success: function(result) {
                console.log(result);
                var str = '<button onclick="addForm()">Tambah data</button>'
                str += '<table border = "1" cellpadding="5" cellspacing="0">'
                str += "<tr>"
                str += "<td>NIK</td>"
                str += "<td>Nama</td>"
                str += "<td>Tanggal_Lahir</td>"
                str += "<td>Alamat</td>"
                str += "<td>Status</td>"
                str += '<td colspan ="2" align="center">Aksi</td>'
                str += "</tr>"
                for (i = 0; i < result.data.length; i++) {
                    str += "<tr>"
                    str += "<td>" + result.data[i].nik + "</td>"
                    str += "<td>" + result.data[i].nama + "</td>"
                    str += "<td>" + result.data[i].tanggal_lahir + "</td>"
                    str += "<td> " + result.data[i].alamat + "</td>"
                    str += "<td>" + result.data[i].status + "</td>"
                    str += '<td><button value="' + result.data[i].id + '" onclick="editForm(this.value)">Edit</button</td>'
                    str += '<td><button value="' + result.data[i].id + '" onclick="deleteForm(this.value)">Hapus</button</td>'
                    str += "</tr>"
                }
                str += "</table>"
                $('#tampildata').html(str)
            }
        })
    }

    function savedata() {
        var isidata = '{"nik":"' + $('#nik').val() + '","nama":"' + $('#nama').val() + '"   ,"tanggal_lahir":"' + $('#tanggal_lahir').val() + '","alamat":"' + $('#alamat').val() + '" ,"status":"' + $('#status').val() + '"}'
        console.log(isidata)
        $.ajax({
            url: 'http://localhost:8000/api/parents/add',
            type: 'post',
            contentType: 'application/json',
            data: isidata,
            success: function(result) {
                console.log(result);
                listdata();
                addForm()
            }
        })
    }

    function editForm(id) {
        $.ajax({
            url: 'http://localhost:8000/api/parents/' + id,
            type: 'get',
            contentType: 'application/json',
            success: function(result) {
                console.log(result);

                $('#id').val(result.data[0].id)
                $('#nik').val(result.data[0].nik)
                $('#nama').val(result.data[0].nama)
                $('#tanggal_lahir').val(result.data[0].tanggal_lahir)
                $('#alamat').val(result.data[0].alamat)
                $('#status').val(result.data[0].status)

                $('#savedata').hide()
                $('#editdata').show()
            }
        })
    }

    function addForm() {
        $('#nik').val('')
        $('#nama').val('')
        $('#tanggal_lahir').val('')
        $('#alamat').val('')
        $('#status').val('')

        $('#savedata').show()
        $('#editdata').hide()
    }

    function editdata() {
        var isidata = '{"id":"' + $('#id').val() + '","nik":"' + $('#nik').val() + '","nama":"' + $('#nama').val() + '","tanggal_lahir":"' + $('#tanggal_lahir').val() + '","alamat":"' + $('#alamat').val() + '","status":"' + $('#status').val() + '"}'
        console.log(isidata)
        $.ajax({
            url: 'http://localhost:8000/api/parents/edit',
            type: 'put',
            contentType: 'application/json',
            data: isidata,
            success: function(result) {
                console.log(result);
                addForm()
                listdata();
            }
        })
    }

    function deleteForm(id) {
        var conf = window.confirm("Yakin mau hapus ? ")
        if (conf) {
            $.ajax({
                url: 'http://localhost:8000/api/parents/delete/' + id,
                type: 'delete',
                contentType: 'application/json',
                success: function(result) {
                    console.log(result);
                    addForm()
                    listdata();
                }
            })
        }
    }
</script>