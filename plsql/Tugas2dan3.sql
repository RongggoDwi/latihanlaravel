--Tugas 2
set serveroutput on;
create or replace FUNCTION batch258.get_total_belanja(in_customer_id batch258.customers.customer_id%type)
RETURN SYS_REFCURSOR
AS
    l_total_belanja SYS_REFCURSOR;
BEGIN
    --get total belanja
    OPEN l_total_belanja FOR
        SELECT c.name, SUM(oi.quantity * oi.unit_price) FROM batch258.customers c
        JOIN batch258.orders o on o.customer_id = c.customer_id
        JOIN batch258.order_items oi on oi.order_id = o.order_id
        WHERE o.customer_id = in_customer_id
        GROUP BY c.name;

    RETURN l_total_belanja;
END;

-- call function
DECLARE
    l_total_belanja SYS_REFCURSOR;
    l_name batch258.customers.name%TYPE;
    l_total number;
BEGIN
    -- get the ref cursor from function
    l_total_belanja := batch258.get_total_belanja(1);
    
    -- process each employee
    LOOP
        FETCH
            l_total_belanja
        INTO
            l_name,
            l_total;
        EXIT
        WHEN l_total_belanja%notfound;
            dbms_output.put_line(l_name  || ' = ' ||  l_total);
        END LOOP;
     -- close the cursor
     CLOSE l_total_belanja;
END;

--Tugas 3
CREATE OR REPLACE FUNCTION batch258.product_laku(
    name_region VARCHAR2)
RETURN SYS_REFCURSOR
AS
    l_product SYS_REFCURSOR;
BEGIN
    --get max qty
    OPEN l_product FOR 
        SELECT product_id ,product_name FROM (
        SELECT * FROM batch258.products
        JOIN batch258.order_items USING (product_id) 
        WHERE batch258.order_items.quantity = (SELECT MAX( batch258.order_items.quantity) FROM batch258.order_items)
        )   JOIN batch258.order_items USING (product_id)
            JOIN batch258.inventories USING (product_id)
            JOIN batch258.warehouses USING (warehouse_id)
            JOIN batch258.locations USING (location_id)
            JOIN batch258.countries USING (country_id)
            JOIN batch258.regions USING (region_id)
            WHERE region_name = name_region
            GROUP BY product_id ,product_name;
         
        RETURN l_product;
END;

--call function
DECLARE
   l_product SYS_REFCURSOR;
   l_product_id batch258.products.product_id%TYPE;
   l_product_name batch258.products.product_name%TYPE;
BEGIN
   -- get the ref cursor
   l_product := batch258.product_laku('Americas'); 
   
   -- process each employee
   LOOP
      FETCH
         l_product
      INTO
        l_product_id,
         l_product_name;
      EXIT
   WHEN l_product%notfound;
      dbms_output.put_line(l_product_id || ' ' || l_product_name);
   END LOOP;
   -- close the cursor
   CLOSE l_product;
END;
