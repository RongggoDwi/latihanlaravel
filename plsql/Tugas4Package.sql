SET SERVEROUTPUT ON;

CREATE OR REPLACE PACKAGE batch258.terbaik
AS
  gc_shipped_status  CONSTANT VARCHAR(10) := 'Shipped';
  gc_pending_status CONSTANT VARCHAR(10) := 'Pending';
  gc_canceled_status CONSTANT VARCHAR(10) := 'Canceled';
  
  -- get net value of a sales order
  FUNCTION get_sales_value
    RETURN SYS_REFCURSOR;

  -- Get net value by manager
  FUNCTION get_manager_by_sales
    RETURN SYS_REFCURSOR;

END terbaik; 

CREATE OR REPLACE PACKAGE BODY batch258.terbaik
AS

  FUNCTION get_sales_value
    RETURN SYS_REFCURSOR
  AS
    in_sales_value SYS_REFCURSOR; 
  BEGIN
  OPEN in_sales_value FOR
    SELECT CONCAT(CONCAT(employees.first_name,' '),employees.last_name) AS "NAMA_LENGKAP",
    SUM(BATCH258.ORDER_ITEMS.QUANTITY) AS "TOTAL_JUAL"
    FROM
      batch258.employees
        JOIN BATCH258.orders ON employees.employee_id = orders.salesman_id
        JOIN BATCH258.order_items ON orders.order_id = order_items.order_id
        WHERE orders.status = gc_shipped_status AND employees.job_title = 'Sales Representative'
        GROUP BY CONCAT(CONCAT(employees.first_name,' '),employees.last_name)
        ORDER BY "TOTAL_JUAL" DESC;
        
    RETURN in_sales_value;
  END;

  FUNCTION get_manager_by_sales
    RETURN SYS_REFCURSOR
  AS
    ln_manager_value SYS_REFCURSOR;
  BEGIN
   OPEN ln_manager_value FOR
        SELECT employees.manager_id, SUM(order_items.quantity) TOTAL_JUAL
        FROM BATCH258.employees
        JOIN BATCH258.orders ON employees.employee_id = orders.salesman_id
        JOIN BATCH258.order_items ON orders.order_id = order_items.order_id
        WHERE orders.status = gc_shipped_status
        GROUP BY employees.manager_id
        ORDER BY TOTAL_JUAL DESC;
        
        RETURN ln_manager_value;
  END;

END terbaik;

DECLARE
   in_sales_value SYS_REFCURSOR;
BEGIN
   -- get the ref cursor from function
   in_sales_value := batch258.terbaik.get_sales_value(); 
   
   dbms_sql.return_result(in_sales_value);
END;

DECLARE
   ln_manager_value SYS_REFCURSOR;
BEGIN
   -- get the ref cursor from function
   ln_manager_value := batch258.terbaik.get_manager_by_sales(); 
   
   dbms_sql.return_result(ln_manager_value);
END;