CREATE OR REPLACE PROCEDURE batch258.get_all_customers
AS
     c_customers SYS_REFCURSOR;
BEGIN
    OPEN c_customers FOR
        SELECT c.customer_id, c.name FROM batch258.customers c
        JOIN batch258.orders o ON c.customer_id = o.customer_id
        WHERE c.customer_id = o.customer_id;
        
    DBMS_SQL.return_result (c_customers);
EXCEPTION
    WHEN OTHERS THEN
    dbms_output.put_line( SQLERRM );
END;

BEGIN
   batch258.get_all_customers;
END;

CREATE OR REPLACE PROCEDURE batch258.get_product_by_region
AS
     p_product SYS_REFCURSOR;
BEGIN
    OPEN p_product FOR
        SELECT pro.product_id, pro.product_name, reg.region_name FROM batch258.products pro
        JOIN batch258.inventories inv ON pro.product_id = inv.product_id
        JOIN batch258.warehouses war ON inv.warehouse_id = war.warehouse_id
        JOIN batch258.locations loc ON loc.location_id = war.location_id
        JOIN batch258.countries coun ON coun.country_id = loc.country_id
        JOIN batch258.regions reg ON reg.region_id = coun.region_id
        WHERE reg.region_name = 'Asia';
        
    DBMS_SQL.return_result (p_product);
EXCEPTION
    WHEN OTHERS THEN
    dbms_output.put_line( SQLERRM );
END;

BEGIN
   batch258.get_product_by_region;
END;

CREATE OR REPLACE PROCEDURE batch258.get_customer_by_region(
    var_region  VARCHAR)    
AS 
    c_customers SYS_REFCURSOR;
BEGIN
    OPEN c_customers FOR 
    SELECT customer_id, name
    FROM batch258.customers
     JOIN batch258.orders USING (customer_id)
     JOIN batch258.order_items USING (order_id)
     JOIN batch258.products USING (product_id)
     JOIN batch258.inventories USING (product_id)
     JOIN batch258.warehouses USING (warehouse_id)
     JOIN batch258.locations USING (location_id)
     JOIN batch258.countries USING (country_id)
     JOIN batch258.regions USING (region_id)
     WHERE region_name = var_region
     ORDER BY customer_id, name;
    
    DBMS_SQL.return_result(c_customers);
END;

EXEC batch258.get_customer_by_region('Asia');

