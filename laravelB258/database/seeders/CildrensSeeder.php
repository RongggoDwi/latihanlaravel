<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CildrensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // for ($i = 1; $i < 10; $i++) {
        // DB::table('cildrens')->insert([
        //     'parent_id' => Str::random(1),
        //     'nik' => Str::random(10),
        //     'nama' => Str::random(10),
        //     'tanggal_lahir' => Str::random(10),
        //     'anak_ke' => Str::random(1),
        // ]);

        // DB::table('cildrens')->insert([
        //     'parent_id'     => 1,
        //     'nik'           => '331382783',
        //     'nama'          => 'Rico Simanjuntak',
        //     'tanggal_lahir' => '11-11-1999',
        //     'anak_ke'        => 1
        // ]);

        // DB::table('cildrens')->insert([
        //     'parent_id'     => 1,
        //     'nik'           => '331382784',
        //     'nama'          => 'Marko Simic',
        //     'tanggal_lahir' => '27-09-2002',
        //     'anak_ke'        => 2
        // ]);

        // DB::table('cildrens')->insert([
        //     'parent_id'     => 2,
        //     'nik'           => '331382785',
        //     'nama'          => 'Bambang Pamungkas',
        //     'tanggal_lahir' => '11-12-2013',
        //     'anak_ke'        => 3
        // ]);
        // }
    }
}
