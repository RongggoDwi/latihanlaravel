<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++) {
            DB::table('Profils')->insert([
                'nama_lengkap' => Str::random(40),
                'email' => Str::random(30) . '@gmail.com',
                'alamat' => Str::random(100),
            ]);
        }
    }
}
