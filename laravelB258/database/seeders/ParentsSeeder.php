<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class ParentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        DB::table('parents')->insert([
            'nik'           => $faker->numberBetween(1, 10000),
            'nama'          => 'Ronggo Dwi Wahono',
            'tanggal_lahir' => '11-10-1996',
            'alamat'        => 'Jogja',
            'status'        => 'Ayah'
        ]);

        DB::table('parents')->insert([
            'nik'           => $faker->numberBetween(1, 10000),
            'nama'          => 'Septy Nindyastuti',
            'tanggal_lahir' => '04-09-1995',
            'alamat'        => 'Karanganyar',
            'status'        => 'Ibu'
        ]);

        // DB::table('Parents')->insert([
        //     'nik' => '3318091101018700',
        //     'nama' => 'Ronggo Dwi Wahono',
        //     'tanggal_lahir' => '10-Nov-1996',
        //     'alamat' => 'Jogja',
        //     'status' => 'Ayah',
        // ]);

        // DB::table('Parents')->insert([
        //     'nik' => '3318091101018700',
        //     'nama' => 'Septy Nindyastuti',
        //     'tanggal_lahir' => '04-Sep-1995',
        //     'alamat' => 'Pati',
        //     'status' => 'Ibu',
        // ]);

        // DB::table('Parents')->insert([
        //     'nik' => '3318091101012000',
        //     'nama' => 'Noah Setiawan',
        //     'tanggal_lahir' => '10-Nov-2000',
        //     'alamat' => 'Jogja',
        //     'status' => 'Anak',
        // ]);
    }
}
