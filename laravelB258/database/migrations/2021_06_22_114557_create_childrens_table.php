<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('childrens', function (Blueprint $table) {
            // $table->increments('id');
            // $table->bigInteger('parent_id', 1);
            // $table->string('nik', 10);
            // $table->string('nama', 20);
            // $table->string('tanggal_lahir', 10);
            // $table->enum('anak_ke', [1, 2, 3, 4, 5]);
            // $table->timestamps();
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('nik');
            $table->string('nama');
            $table->string('tanggal_lahir');
            $table->integer('anak_ke');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('childrens');
    }
}
