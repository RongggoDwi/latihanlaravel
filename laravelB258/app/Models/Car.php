<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $table = 'cars';

    public function profil_relation()
    {
        return $this->belongsTo('App\Models\Profil', 'profil_id');
    }
}
