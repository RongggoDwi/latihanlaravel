<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gender;

class GenderController extends Controller
{
    public function getData()
    {
        $data = Gender::with('parents_relation')->get();
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function getDataById($id)
    {
        $data = Gender::where('id', $id)->get();
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "300 - No Data";
        }
        return response($res);
    }

    public function saveData(Request $request)
    {
        $data = new Gender();
        $data->jenis_kelamin = $request->jenis_kelamin;
        if ($data->save()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - Service Unavaible";
        }
        return response($request);
    }

    public function updateData(Request $request, $id)
    {
        $data = Gender::find($id);
        $data->jenis_kelamin = $request->jenis_kelamin;
        if ($data->save()) {
            $res['status'] = "Insert Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - Service Unavaible";
        }
        return response($request);
    }

    public function deleteData($id)
    {
        $data = Gender::find($id);
        if ($data->delete()) {
            $res['status'] = "200 - Delete Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - Service Unavaible";
        }
        return response($res);
    }
}
