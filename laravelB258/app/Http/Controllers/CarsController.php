<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use App\Models\Profil;
use Illuminate\Support\Facades\DB;

class CarsController extends Controller
{
    public function index()
    {
        $data = Car::with('profil_relation')->get();
        return view('car.index', ['data' => $data]);
    }

    public function indexcars()
    {
        $data = Car::where('brand', 'mercedes')->orderBy('model')->get();
        return response($data);
    }

    public function getCars()
    {
        $data = Car::with('profil_relation')->get(); // Json result
        return response($data);
        // return view('car.index', ['data' => $data]);
    }

    public function getCarsNative()
    {
        $data = DB::select("SELECT * FROM cars c LEFT JOIN Profils p on p.id = c.profil_id"); // query native
        return response($data);
        // return view('car.index', ['data' => $data]);
    }

    function add()
    {
        $profil = Profil::all();
        return view('car.add', ['profil' => $profil]);
    }

    function simpan(Request $request)
    {
        $data = new Car();
        $data->profil_id = $request->profil_id;
        $data->model = $request->model;
        $data->brand = $request->brand;
        $data->cc = $request->cc;
        $data->valve = $request->valve;
        $data->year = $request->year;

        if ($data->save()) {
            return redirect('/car');
        } else {
            return redirect('/car/add');
        }
    }
}
