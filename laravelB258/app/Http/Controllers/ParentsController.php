<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parents;

class ParentsController extends Controller
{
    function index()
    {
        $data = Parents::all();
        return view('parents.index', ['data' => $data]);
    }
}
