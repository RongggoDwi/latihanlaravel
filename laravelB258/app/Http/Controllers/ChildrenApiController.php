<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Children;
use App\Models\Parents;

class ChildrenApiController extends Controller
{
    public function getAllChildDataRelasiParent()
    {
        $data = Children::with('parents_relation')->get();
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }


    public function saveData(Request $req)
    {
        $data = new Children();
        $data->parent_id = $req->parent_id;
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->anak_ke = $req->anak_ke;
        if ($data->save()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function updateData(Request $req)
    {
        $data = Children::find($req->id);
        $data->parent_id = $req->parent_id;
        $data->nik = $req->nik;
        $data->nama = $req->nama;
        $data->tanggal_lahir = $req->tanggal_lahir;
        $data->anak_ke = $req->anak_ke;
        if ($data->save()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function getData($id)
    {
        $data = Children::where('id', $id)->get();
        if (count($data) > 0) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }

    public function deleteData($id)
    {
        $data = Children::find($id);
        if ($data->delete()) {
            $res['status'] = "200 - Success";
            $res['data'] = $data;
        } else {
            $res['status'] = "503 - No Data";
        }
        return response($res);
    }
}
