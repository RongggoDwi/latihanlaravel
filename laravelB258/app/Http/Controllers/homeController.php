<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function index()
    {
        $data = array(1, 3, 5, 6, 7);
        $data2 = array('CC' => 2200, 'valve' => 3000, 'wheel' => 4000);
        return view('home.index', ['data' => $data, 'data2' => $data2]);
    }
}
