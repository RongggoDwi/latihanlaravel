<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profil;

class ProfilController extends Controller
{
    function index()
    {
        $data = Profil::all();
        return view('profil.index', ['data' => $data]);
    }

    function add()
    {
        return view('profil.add');
    }

    function added(Request $request)
    {
        $data = new Profil();
        $data->nama_lengkap = $request->nama_lengkap;
        $data->email = $request->email;
        $data->alamat = $request->alamat;

        if ($data->save()) {
            return redirect('/profil');
        } else {
            return redirect('/profil/add');
        }
    }

    public function edit($id)
    {
        $data = Profil::where('id', $id)->get();
        return view('profil.edit', ['data' => $data]);
    }

    public function editdata(Request $request)
    {
        $data = Profil::find($request->id);
        $data->nama_lengkap = $request->nama_lengkap;
        $data->email = $request->email;
        $data->alamat = $request->alamat;

        if ($data->save()) {
            return redirect('/profil');
        } else {
            return redirect('/profil/edit');
        }
    }

    public function datadelete($id)
    {
        $data = Profil::where('id', $id)->get();
        return view('profil.datadelete', ['data' => $data]);
    }

    public function deleted(Request $request)
    {
        $data = Profil::find($request->id);

        if ($data->delete()) {
            return redirect('/profil');
        } else {
            return redirect('/profil/datadelete');
        }
    }
}
