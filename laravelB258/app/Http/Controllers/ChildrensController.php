<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Children; // wes kan ??
use App\Models\Parents;


class ChildrensController extends Controller
{
    public function index()
    {
        $data = Children::with('parents_relation')->get();
        // echo "<pre>";
        // print_r($data);
        // print_r($data);
        return view('children.index', ['data' => $data]);
    }

    public function add()
    {
        $parents = Parents::all();
        return view('children.add', ['parents' => $parents]);
    }

    function simpan(Request $request)
    {
        $data = new Children();
        $data->parent_id = $request->parent_id;
        $data->nik = $request->nik;
        $data->nama = $request->nama;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->anak_ke = $request->anak_ke;

        if ($data->save()) {
            return redirect('/children');
        } else {
            return redirect('/children/add');
        }
    }
}
