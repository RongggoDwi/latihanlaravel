<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'homeController@index');

Route::get('/profil', 'ProfilController@index');

Route::get('/parents', 'ParentsController@index');

Route::get('/children', 'ChildrensController@index');
Route::get('/children/add', 'ChildrensController@add');
Route::post('/children/simpan', 'ChildrensController@simpan');

Route::get('/profil/add', 'ProfilController@add');
Route::post('/profil/added', 'ProfilController@added');

Route::get('/profil/edit/{id}', 'ProfilController@edit');
Route::post('/profil/editdata', 'ProfilController@editdata');

Route::get('/profil/datadelete/{id}', 'ProfilController@datadelete');
Route::post('/profil/deleted', 'ProfilController@deleted');

Route::get('/car', 'CarsController@index');
Route::get('/car/add', 'CarsController@add');
Route::post('/car/simpan', 'CarsController@simpan');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
