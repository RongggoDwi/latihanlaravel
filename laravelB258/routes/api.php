<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/children', 'ChildrenApiController@getAllChildDataRelasiParent');
Route::get('/children/{id}', 'ChildrenApiController@getData');
Route::post('/children/add', 'ChildrenApiController@saveData');
Route::put('/children/edit', 'ChildrenApiController@updateData');
Route::delete('/children/delete/{id}', 'ChildrenApiController@deleteData');

Route::get('/gender', 'GenderController@getData');
Route::get('/gender/{id}', 'GenderController@getDataById');
Route::post('gender', 'GenderController@saveData');
Route::put('gender/{id}', 'GenderController@updateData');
Route::delete('gender/{id}', 'GenderController@deleteData');

Route::get('/parents', 'ParentsApiController@getAllParents');
Route::get('/parents/{id}', 'ParentsApiController@getData');
Route::post('/parents/add', 'ParentsApiController@saveData');
Route::put('/parents/edit', 'ParentsApiController@updateData');
Route::delete('/parents/delete/{id}', 'ParentsApiController@deleteData');

Route::get('/getCars', 'CarsController@getCars');
Route::get('/getCarsNative', 'CarsController@getCarsNative');
Route::get('/indexcars', 'CarsController@indexcars');
