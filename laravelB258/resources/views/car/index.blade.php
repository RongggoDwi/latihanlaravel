@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('content')
<br>
<!-- @foreach($data as $d)
<p>{{ $d->model }} | {{ $d->model }} | {{ $d -> profil_relation -> nama_lengkap}}</p>
@endforeach -->
<button onclick="add()">Add</button>
<table class="table table-striped table-hover" border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th>id</th>
            <th>model</th>
            <th>brand</th>
            <th>cc</th>
            <th>valve</th>
            <th>year</th>
            <th>owner</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $d)
        <tr>
            <td>{{$d->id}}</td>
            <td>{{$d->model}}</td>
            <td>{{$d->brand}}</td>
            <td>{{$d->cc}}</td>
            <td>{{$d->valve}}</td>
            <td>{{$d->year}}</td>
            <td>{{$d->profil_relation->nama_lengkap}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    function add() {

        $.ajax({
            url: '/car/add',
            type: 'get',
            contentType: 'html',
            success: function(result) {
                $('#mymodal').modal('show');
                $('#judulmodal').html('form tambah car');
                $('.modal-body').html(result);
            }
        })
        // window.open('/profile/add/', '_self')
    }
</script>

@stop