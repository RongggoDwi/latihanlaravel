@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('content')
<br>

<table class="table table-striped table-hover" border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th>NIK</th>
            <th>NAMA</th>
            <th>TANGGAL LAHIR</th>
            <th>ALAMAT</th>
            <th>STATUS</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $d)
        <tr>
            <td>{{$d->nik}}</td>
            <td>{{$d->nama}}</td>
            <td>{{$d->tanggal_lahir}}</td>
            <td>{{ Str::limit($d->alamat, 40, ' (...)') }}</td>
            <td>{{$d->status}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop