<form action="/children/simpan" method="post">
    {{csrf_field()}}
    <table>
        <tr>
            <td>PARENT_ID</td>
            <td><select name="parent_id">
                    @foreach ($parents as $p)
                    <option value="{{ $p->id }}">{{$p->nama}}</option>
                    @endforeach
                </select></td>
        </tr>
        <tr>
            <td>NIK</td>
            <td><input type="text" name="nik" class="form-control"></td>
        </tr>
        <tr>
            <td>NAMA</td>
            <td><textarea type="text" name="nama" class="form-control"></textarea></td>
        </tr>
        <tr>
            <td>TANGGAL_LAHIR</td>
            <td><textarea type="text" name="tanggal_lahir" class="form-control"></textarea></td>
        </tr>
        <tr>
            <td>ANAK_KE</td>
            <td><textarea type="text" name="anak_ke" class="form-control"></textarea></td>
        </tr>
    </table>
    <div class="modal-footer">
        <input type="button" value="Batal" data-dismiss="modal" class="btn btn-danger" />
        <input type="submit" value="Simpan" class="btn btn-success" />
    </div>
</form>