@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('content')
<br>

<button onclick="add()">Add</button>
<table class="table table-striped table-hover" border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th>id</th>
            <th>PARENT_ID</th>
            <th>NIK</th>
            <th>NAMA</th>
            <th>TANGGAL LAHIR</th>
            <th>ANAK_KE</th>
            <th>NAMA ORTU</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $d)
        <tr>
            <td>{{$d->id}}</td>
            <td>{{$d->parent_id}}</td>
            <td>{{$d->nik}}</td>
            <td>{{$d->nama}}</td>
            <td>{{$d->tanggal_lahir}}</td>
            <td>{{$d->anak_ke}}</td>
            <td>{{$d->parents_relation->nama}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    function add() {

        $.ajax({
            url: '/children/add',
            type: 'get',
            contentType: 'html',
            success: function(result) {
                $('#mymodal').modal('show');
                $('#judulmodal').html('form tambah anak');
                $('.modal-body').html(result);
            }
        })
        // window.open('/profile/add/', '_self')
    }
</script>

@stop