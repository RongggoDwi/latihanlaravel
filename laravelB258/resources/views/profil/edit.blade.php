@foreach ($data as $item)
<form action="/profil/editdata" method="post">
    {{csrf_field()}}
    <input type="hidden" name="id" value="{{ $item->id }}">
    <table>
        <tr>
            <td>Nama Lengkap</td>
            <td><input type="text" name="nama_lengkap" value="{{ $item->nama_lengkap }}"></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" value="{{ $item->email }}"></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td><textarea name="alamat">{{ $item->alamat }}</textarea></td>
        </tr>
    </table>
    <div class="modal-footer">
        <input type="submit" value="submit" class="btn btn-success" />
    </div>
    @endforeach