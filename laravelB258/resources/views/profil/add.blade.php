<form action="/profil/added" method="POST">
    {{csrf_field()}}
    <table>
        <tr>
            <td>NAMA LENGKAP</td>
            <td><input type="text" name="nama_lengkap" class="form-control"></td>
        </tr>
        <tr>
            <td>EMAIL</td>
            <td><input type="text" name="email" class="form-control"></td>
        </tr>
        <tr>
            <td>ALAMAT</td>
            <td><textarea type="text" name="alamat" class="form-control"></textarea></td>
        </tr>
    </table>
    <div class="modal-footer">
        <input type="button" value="Batal" data-dismiss="modal" class="btn btn-danger" />
        <input type="submit" value="Simpan" class="btn btn-success" />
    </div>
</form>