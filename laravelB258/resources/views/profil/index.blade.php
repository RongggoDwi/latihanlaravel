@extends('layout.layout')
@section('title', 'XSIS ACADEMY')
@section('content')
<br>

<button onclick="add()">Add</button>
<table class="table table-striped table-hover" border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th>NO.</th>
            <th>NAMA_LENGKAP</th>
            <th>EMAIL</th>
            <th>ALAMAT</th>
            <th>AKSI</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $d)
        <tr>
            <td>{{$d->id}}</td>
            <td>{{$d->nama_lengkap}}</td>
            <td>{{$d->email}}</td>
            <td>{{ Str::limit($d->alamat, 40, ' (...)') }}</td>
            <td>
                <button value="{{ $d->id}}" onclick="edit(this.value)">Edit</button>
                <button value="{{ $d->id}}" onclick="delete_(this.value)">Hapus</button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    function add() {

        $.ajax({
            url: '/profil/add',
            type: 'get',
            contentType: 'html',
            success: function(result) {
                $('#mymodal').modal('show');
                $('#judulmodal').html('form tambah profil');
                $('.modal-body').html(result);
            }
        })
        // window.open('/profile/add/', '_self')
    }

    function edit(id) {
        $.ajax({
            url: '/profil/edit/' + id,
            type: 'get',
            contentType: 'html',
            success: function(result) {
                $('#mymodal').modal('show');
                $('#judulmodal').html('form edit profil');
                $('.modal-body').html(result);
            }
        })

        // window.open('/profile/edit/' + id, '_self')
    }

    function delete_(id) {
        $.ajax({
            url: '/profil/datadelete/' + id,
            type: 'get',
            contentType: 'html',
            success: function(result) {
                $('#mymodal').modal('show');
                $('#judulmodal').html('form delete profil');
                $('.modal-body').html(result);
            }
        })
        // window.open('/profile/delete/' + id, '_self')
    }
</script>



@stop