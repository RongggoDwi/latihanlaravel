<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Doctrine\DBAL\Driver\PDOConnection;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;

class ApiController extends Controller
{
    // public function getAllContact(){
    //     $data = DB::select('SELECT * FROM batch258.customers');

    //     return $data;
    // }

    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function getAllContact()
    {
        $data = DB::select('SELECT * FROM batch258.contacts');
        // return $data;
        echo "<pre>";
        print_r($data);
    }

    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    public function show()
    {
        $this->soapWrapper->add('Converter', function ($service) {
            $service
                ->wsdl('http://www.learnwebservices.com/services/tempconverter?wsdl')
                ->trace(true)
                ->classmap([
                    celsiusToFahrenheitRequest::class,
                ]);
        });

        // Without classmap
        $response = $this->soapWrapper->call('Converter.celsiusToFahrenheitRequest', [
            'TemperatureInCelsius' => '37.7',
        ]);

        echo '<pre>';
        print_r($response);
        exit;
    }
}
